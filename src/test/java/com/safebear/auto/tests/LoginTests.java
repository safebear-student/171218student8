package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest{

    @Test
    public void validLogin(){
        // 1:Action Go to login URL
        driver.get(Utils.getUrl());

        // 1: Expected On Login page
        Assert.assertEquals(loginPage.getTitle(),loginPage.getExpectedPageTitle());

        //2: Action Enter valid login info and submit form
        loginPage.login("tester","letmein");

        //2: Expected Check we're on the Tools Page
        Assert.assertEquals(loginPage.getTitle(), "Tools Page","page title is not as defined, or I'm not logged in");

        //2: Expected success message is shown
        Assert.assertTrue(toolsPage.getLoginSuccessMessage().contains("Success"));

    }

    @Test
    public void failedLogin(){

    }

}
