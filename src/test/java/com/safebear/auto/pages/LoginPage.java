package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    private LoginPageLocators loginPageLocators = new LoginPageLocators();
    @NonNull WebDriver driver;
    String expectedPageTitle="Login Page";

    public String getTitle() {
        return driver.getTitle();
    }

    public String getExpectedPageTitle(){
        return expectedPageTitle;
    }

    public void enterUsername(String uname) {
        driver.findElement(loginPageLocators.getUsernameFieldLocator()).sendKeys(uname);
    }

    public void enterPassword(String pwrd) {
        driver.findElement(loginPageLocators.getPasswordLocator()).sendKeys(pwrd);
    }

    public void clickSubmitButton() {
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();
    }

    public void login(String uname, String pwrd) {
        enterUsername(uname);
        enterPassword(pwrd);
        clickSubmitButton();
    }
}
