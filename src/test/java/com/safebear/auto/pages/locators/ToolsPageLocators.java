package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    By loginSuccessMessage = By.xpath(".//p[2]/b/.");
}
