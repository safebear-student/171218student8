package com.safebear.auto.syntax;


import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee(){
        Employee hannah = new Employee();
        Employee bob = new Employee();

        hannah.employ();
        bob.fire();

        System.out.println("emp "+hannah.getEmployed());
    }

}
